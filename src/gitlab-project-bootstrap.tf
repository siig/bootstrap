# Bootstraps the main repo for this group
resource "gitlab_project" "bootstrap" {
  name                   = "bootstrap"
  description            = "Bootstraps the main repo for this group"
  visibility_level       = "public"
  default_branch         = "main"
  shared_runners_enabled = true
  namespace_id           = 8345471
}

########################
## BRANCH PROTECTIONS ##
########################

resource "gitlab_branch_protection" "bootstrap_main" {
  project            = gitlab_project.bootstrap.id
  branch             = "main"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "bootstrap_devel" {
  project            = gitlab_project.bootstrap.id
  branch             = "devel"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}

#######################
## PROJECT VARIABLES ##
#######################

resource "gitlab_project_variable" "bootstrap_gitlab_token" {
  project = gitlab_project.bootstrap.id
  key     = "GITLAB_TOKEN"
  # TODO: Remove this once Hashicorp Vault has been deployed
  value             = var.gitlab_token
  protected         = false
  masked            = true
  environment_scope = "*"
}

resource "gitlab_project_variable" "bootstrap_tfe_token" {
  project = gitlab_project.bootstrap.id
  key     = "TFE_TOKEN"
  # TODO: Remove this once Hashicorp Vault has been deployed
  value             = var.tfe_token
  protected         = false
  masked            = true
  environment_scope = "*"
}
