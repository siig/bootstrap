# Contains Dockerfile for creating image with Terraform and other needed tools
resource "gitlab_project" "gitlab_runner_terraform" {
  name                   = "gitlab-runner-terraform"
  description            = "Dockerfile with Terraform and other necessary tools installed"
  namespace_id           = 8345471
  visibility_level       = "public"
  default_branch         = "main"
  shared_runners_enabled = true
}

## BRANCH PROTECTIONS ##

resource "gitlab_branch_protection" "runner_tf_main" {
  project            = gitlab_project.gitlab_runner_terraform.id
  branch             = "main"
  push_access_level  = "developer"
  merge_access_level = "developer"
}

resource "gitlab_branch_protection" "runner_tf_devel" {
  project            = gitlab_project.gitlab_runner_terraform.id
  branch             = "devel"
  push_access_level  = "developer"
  merge_access_level = "developer"
}

## PROJECT VARIABLES ##

resource "gitlab_project_variable" "runner_tf_gitlab_token" {
  project = gitlab_project.gitlab_runner_terraform.id
  key     = "GITLAB_TOKEN"
  # TODO: Remove this once Hashicorp Vault has been deployed
  value     = var.gitlab_token
  protected = true
  masked    = true
}
