# Contains the base infrastructure for the entire Siig group
resource "gitlab_project" "main" {
  name                   = "siig"
  description            = "Everything related to my technical life. Some of it's private, but most of it's open source!"
  visibility_level       = "public"
  default_branch         = "main"
  shared_runners_enabled = true
  namespace_id           = 8345471
}

########################
## BRANCH PROTECTIONS ##
########################

resource "gitlab_branch_protection" "main_main" {
  project            = gitlab_project.main.id
  branch             = "main"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "main_devel" {
  project            = gitlab_project.main.id
  branch             = "devel"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}

#######################
## PROJECT VARIABLES ##
#######################

resource "gitlab_project_variable" "main_gitlab_token" {
  project = gitlab_project.main.id
  key     = "GITLAB_TOKEN"
  # TODO: Remove this once Hashicorp Vault has been deployed
  value             = var.gitlab_token
  protected         = false
  masked            = true
  environment_scope = "*"
}

resource "gitlab_project_variable" "main_tfe_token" {
  project = gitlab_project.main.id
  key     = "TFE_TOKEN"
  # TODO: Remove this once Hashicorp Vault has been deployed
  value             = var.tfe_token
  protected         = false
  masked            = true
  environment_scope = "*"
}
