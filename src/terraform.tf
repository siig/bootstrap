terraform {
  required_version = ">= 0.12"

  backend "remote" {
    organization = "siig"

    workspaces {
      name = "bootstrap"
    }
  }

  required_providers {
    gitlab = "~> 2.10"
    tfe    = "~> 0.15.0"
  }
}

resource "random_integer" "project_id" {
  min = 001
  max = 999
}
