resource "tfe_workspace" "bootstrap" {
  name         = "bootstrap"
  organization = "Siig"
  operations   = false
}

resource "tfe_workspace" "main_dev" {
  name         = "siig-dev"
  organization = "Siig"
  operations   = false
}

resource "tfe_workspace" "main_prod" {
  name         = "siig-prod"
  organization = "Siig"
  operations   = false
}
