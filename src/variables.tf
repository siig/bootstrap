variable "gitlab_token" {
  description = "Token to authorize with GitLab"
}

variable "tfe_token" {
  description = "Token to authorize with Terraform Cloud"
}